FROM eclipse-temurin:17-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENV JAVA_OPTS "-XX:MaxRAMPercentage=75.0 -XX:+HeapDumpOnOutOfMemoryError -XX:+ExitOnOutOfMemoryError"
CMD exec java ${JAVA_OPTS} -jar /app.jar