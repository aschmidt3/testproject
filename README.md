
# Zast-Import-Csv 
# Trigger 
## Voraussetzungen

- IntelliJ Idea (_Community Edition reicht aus_)
- Docker `docker --version`
- JAVA (JDK Version 11 oder höher) `java --version`
- Maven (_optional, weil kommt automatisch mit IntelliJ_) `mvn --version`
- [Doppler](https://docs.doppler.com/docs/cli) (_optional_) `doppler --version`

## Erste Schritte



## Konfiguration (Teil A)

- `pom.xml` Maven Konfigurationsdatei
- `application-*.yml` SpringBoot-Konfigurationsdateien für Lokal-, Test-, CI/CD- und Produktiv-Umgebung
- `logback.xml` Logger-Konfigurationsdatei
- `Application` ggf. anpassen (siehe SpringBoot)

## Konfiguration (Teil B - optional)

- `Dockerfile` Docker Konfigurationsdatei. Die Google-Cloud führt den Microservice als Dockercontainer aus.
- `docker*.(sh|bat)` Scripte zum Bauen, Pushen und Ausführen des Dockercontainers in der lokalen Umgebung
- `apigen.py` & `.gitlab-ci.yml` Pipeline Scripte (_DevOps_)

## SpringBoot

- `Application` startet die Produktivanwendung. (_wird lokal nicht funktionieren_)
- `infrastructure/config` beinhaltet alle Spring-Konfigurationen
- `SwaggerConfig` generiert AUTOMATISCH die [Swagger-Definition](http://localhost:8080/swagger-ui/index.html)
- `LocalApplication` startet die Anwendung lokal.
  - `PostgresInitializer` startet eine lokale Instanz von Postgresql
  - `RabbitMqInitializer` startet eine lokale Instanz von RabbitMQ
  - `DopplerPropertyInitializer` Lädt Doppler-Secrets als Properties in den Spring-Context.
  - ...weitere `*Initializer` denkbar. Siehe [Testcontainers](https://www.testcontainers.org/)

## Clean Architecture

- Urheber ist [Uncle Bob](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
- `domain` (_Sprache des Kunden, fachlich_)
- `infrastructure` (_Sprache des Entwicklers, technisch_)
- `application` (_Schnittstelle zwischen `domain` und `application`_)
- `persistence` (_Kompromiss, verletzt Clean Architecture, aber alles andere wäre overkill_)
- `common` (_Helferklassen ohne klare Zugehörigkeit_)

## Test-Tools & -Frameworks

- [WireMock](https://wiremock.org/) Mocken von Microservices 
- [Testcontainers](https://www.testcontainers.org/) Mocken von Datenbanken & Message-Brokern
- [Mockito](https://site.mockito.org/) Mocken in Unit-Tests

## SonarCube

[SonarCube](https://docs.sonarqube.org/latest/setup/get-started-2-minutes/) lokal im Docker Container ausführen:
`docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest`
