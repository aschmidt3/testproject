#!/bin/sh
set -e
mvn clean package
docker build -t registry.gitlab.com/rehavital/teams/microservice-devs/test "$(dirname "$0")"
