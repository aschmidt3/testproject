#!/bin/sh
set -e
docker login registry.gitlab.com
docker build -t registry.gitlab.com/rehavital/teams/microservice-devs/test .
docker push registry.gitlab.com/rehavital/teams/microservice-devs/test
